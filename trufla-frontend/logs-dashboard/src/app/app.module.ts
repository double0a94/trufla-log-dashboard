import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";

import { Ng2SmartTableModule } from "ng2-smart-table";
import { Ng2CompleterModule } from "ng2-completer";
import { MatIconModule } from "@angular/material/icon";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { FlashMessagesModule } from "ngx-flash-messages";

import { AppComponent } from "./app.component";
import { LogTableComponent } from "./components/log-table/log-table.component";

import { CoreModule } from "./core/core.module";

@NgModule({
  declarations: [AppComponent, LogTableComponent],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    CoreModule.forRoot(),
    Ng2SmartTableModule,
    Ng2CompleterModule,
    MatIconModule,
    NoopAnimationsModule,
    FlashMessagesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
