import {
  ModuleWithProviders,
  NgModule,
  Optional,
  SkipSelf
} from "@angular/core";
import { CommonModule } from "@angular/common";
import { of as observableOf } from "rxjs";

// import the logs interface API functions
import { LogsApi } from "./data/logs";

import { LogsService } from "./service/logs.service";

import { throwIfAlreadyLoaded } from "./import-guard";

const DATA_SERVICES = [{ provide: LogsApi, useClass: LogsService }];

export const CORE_PROVIDERS = [...DATA_SERVICES];

@NgModule({
  imports: [CommonModule],
  declarations: []
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, "CoreModule");
  }

  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: CoreModule,
      providers: [...CORE_PROVIDERS]
    };
  }
}
