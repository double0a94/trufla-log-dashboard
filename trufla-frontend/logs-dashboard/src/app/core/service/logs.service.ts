import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { LogsApi } from "../data/logs";

@Injectable({
  providedIn: "root"
})
export class LogsService implements LogsApi {
  /**
   * This service is responsible for all CRUD requests
   */

  // backend URI and endpoint API
  private backendUrl = "http://localhost:3000/api/";
  private endpoint = "log";

  constructor(private http: HttpClient) {}

  //  Get all logs
  getAllLogs() {
    return this.http.get(`${this.backendUrl + this.endpoint}`);
  }

  // Get a certain log
  getLog(id: Object) {
    console.log(id);
    return this.http.get(`${this.backendUrl + this.endpoint}/${id}`);
  }

  // Add a new Log
  addLog(newLog: Object) {
    console.log("from the service newLog:", newLog);
    return this.http.post(`${this.backendUrl + this.endpoint}`, {
      log: newLog
    });
  }
}
