export abstract class LogsApi {
  abstract getAllLogs();

  abstract addLog(Object);

  abstract getLog(Object);
}
