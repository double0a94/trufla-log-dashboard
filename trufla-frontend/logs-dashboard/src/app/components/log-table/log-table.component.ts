import { Component, OnInit } from "@angular/core";

import { LogsApi } from "../../core/data/logs";
import { LogsService } from "../../core/service/logs.service";

import { FlashMessagesService } from "ngx-flash-messages";

@Component({
  selector: "app-log-table",
  templateUrl: "./log-table.component.html",
  styleUrls: ["./log-table.component.sass"]
})
export class LogTableComponent implements OnInit {
  /**
   *  ng2-smart-table package helps in pagination, sorting,adding,editing ,data in the table
   *  so we need to set the settings for the columns and path it to the <ng2-smart-table> tag
   */
  settings = {
    actions: {
      add: true,
      edit: false,
      delete: false
    },
    add: {
      confirmCreate: true,
      addButtonContent: "<i class='material-icons'>add</i>",
      createButtonContent: "<i class='material-icons'>done</i>",
      cancelButtonContent: "<i class='material-icons'>cancel</i>"
    },
    columns: {
      title: {
        title: "Title"
      },
      description: {
        title: "Description"
      },
      statusCode: {
        title: "Status Code"
      },
      logPath: {
        title: "Log Path",
        filter: false
      }
    }
  };

  data = [];

  constructor(
    private service: LogsService,
    private flashMessagesService: FlashMessagesService
  ) {
    // Get all the logs when the component is loaded
    this.service.getAllLogs().subscribe(
      (response: any) => {
        // save the data locally and put it to the table
        console.log(response);

        // Check if we got a success response and a data
        if (response.data && response.data.length > 0) {
          this.data = response.data;

          // Show flash Message
          this.flashMessagesService.show(response.message, {
            classes: ["alert", "alert-success"]
          });
        } else {
          console.log(response.message);
          // An error occured
          this.flashMessagesService.show(
            "An error occured please try again later",
            {
              classes: ["alert", "alert-danger"]
            }
          );
        }
      },
      err => {
        console.log(err);
        this.flashMessagesService.show(err.error.message, {
          classes: ["alert", "alert-danger"]
        });
      }
    );
  }

  ngOnInit() {}

  // Add a new log
  createConfirm(event): void {
    console.log(event);
    // Call the add log service
    this.service.addLog(event.newData).subscribe(
      (response: any) => {
        // Show flash Message
        this.flashMessagesService.show(response.message, {
          classes: ["alert", "alert-success"]
        });

        // Add dynamicly to the table
        event.confirm.resolve();
      },
      err => {
        // formating error message
        const errMessageArr = err.error.message.split(",");
        const errMessageFormated = errMessageArr.map(
          errMessage => "* " + errMessage
        );
        const errMessageStr = errMessageFormated.join("\n");

        console.log(errMessageStr);

        // Show flash Message
        this.flashMessagesService.show(errMessageStr, {
          classes: ["alert", "alert-danger"]
        });
      }
    );
  }

  // // Delete log
  // onDeleteConfirm(event): void {

  // }

  // //edit log
  // editConfirm(event): void {

  // }
}
