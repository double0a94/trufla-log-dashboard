import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// Import components
import { LogTableComponent } from "./components/log-table/log-table.component";

const routes: Routes = [{ path: "", component: LogTableComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
