# Log Backend with hapi

## To Run this project you have two ways:

1. Using Docker:
   - Run `docker-compose build`.
   - then `docker-compose up`.
2. Using npm:
   - Run `npm install`.
   - Run in the project directory `npm start` or `nodemon start` if you have a gloable installation of nodemon.

## To run the tests

- Run `npm test`

## To run swagger API documentation:

1. You first should have a running project's server and mongodb instances.
2. Then head over to `http:\\localhost:3000\documentation`
