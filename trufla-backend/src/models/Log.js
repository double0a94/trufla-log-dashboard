const mongoose = require("mongoose");

const logModel = mongoose.Schema(
  {
    title: { type: String, required: true },
    description: { type: String, required: true },
    logPath: { type: String, required: true },
    statusCode: { type: Number, required: true }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Log", logModel);
