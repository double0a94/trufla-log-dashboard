const api = require("./api/log");

const routes = [
  {
    // Route used for testing purpose
    method: "GET",
    path: "/",
    handler: (request, h) => {
      return { success: true };
    }
  },
  {
    // Get all logs
    method: "GET",
    path: "/api/log",
    options: api.all
  },
  {
    // Add a new log
    method: "POST",
    path: "/api/log",
    options: api.add
  },
  {
    // Get a specific log with id
    method: "GET",
    path: "/api/log/{id}",
    options: api.getOne
  }
];

module.exports = routes;
