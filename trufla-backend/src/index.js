require("dotenv").config();

const Hapi = require("hapi");
const routes = require("./routes");

// swagger helper packages
const Inert = require("@hapi/inert");
const Vision = require("@hapi/vision");
const HapiSwagger = require("hapi-swagger");

require("./utils/database");

const server = Hapi.server({
  port: process.env.PORT || 3001,
  host: process.env.HOST || "localhost",
  routes: { cors: true }
});

const startServer = async () => {
  try {
    // add the routes
    routes.forEach(route => {
      server.route(route);
    });

    // Get the configuration options for swagger
    const swaggerOptions = await require("./config/swaggerConfig");

    // register the swagger docs
    await server.register([
      Inert,
      Vision,
      {
        plugin: HapiSwagger,
        options: swaggerOptions.options
      }
    ]);

    // start the server
    await server.start();

    console.log(`Server running at: ${server.info.uri}`);
  } catch (err) {
    console.log(err);
    Boom.badImplementation(err);
  }
};

startServer();

module.exports = server;
