const Log = require("./../models/Log");
const Boom = require("@hapi/boom");
const Joi = require("joi");

var ObjectId = require("mongoose").Types.ObjectId;

const logApi = {
  // Get all Logs
  all: {
    auth: false, // turn hapijs auth to false
    tags: ["api"], // to be visable to swagger
    async handler(request, h) {
      // API request handeler
      const logs = await Log.find({})
        .sort({ createdAt: "desc" })
        .exec();

      // Check if logs do exists
      if (logs.length <= 0) {
        throw Boom.notFound("No logs found");
      } else {
        return {
          message: "Logs retrieved successfully",
          data: logs
        };
      }
    }
  },
  add: {
    auth: false,
    tags: ["api"], // to be visable to swagger
    validate: {
      // validate fields with joi (also compatatible with swagger)
      payload: {
        log: {
          title: Joi.string()
            .required()
            .label("Title field is required."),
          description: Joi.string()
            .required()
            .label("Description field is required."),
          logPath: Joi.string()
            .required()
            .label("Log Path field is required."),
          statusCode: Joi.number()
            .required()
            .label("Status Code field is required and must be a number.")
        }
      },
      failAction: async (request, h, err) => {
        // use failAction in this route to get the custom messages

        // get the Error message label only and sent it back
        const errMessages = err.details.map(x => x.context.label);
        throw Boom.badRequest(errMessages, []);
      },
      options: {
        // allows joi to check the rest of the messages
        abortEarly: false
      }
    },
    async handler(request, h) {
      try {
        const newLog = request.payload.log;

        // Create a new Log Object with the new data
        const log = await new Log(newLog);

        // save the new log to the database
        const savedLog = await log.save();

        if (!savedLog) {
          throw Boom.badData("Log saving failed");
        } else {
          return { message: "Log created successfully", log };
        }
      } catch (err) {
        throw Boom.badImplementation(
          "An error occured please try again later",
          err
        );
      }
    }
  },
  getOne: {
    auth: false,
    tags: ["api"], // to be visable to swagger
    validate: {
      params: {
        id: Joi.string().required()
      }
    },
    async handler(request, h) {
      const logId = request.params.id;

      // check if the passed id is a hex string (BSON ObjectId)
      if (ObjectId.isValid(logId)) {
        // Get the required log
        const log = await Log.findOne({ _id: ObjectId(logId) }).exec();

        // if no error then return the gather log (if found)
        if (!log) {
          throw Boom.notFound("No log found", log);
        } else {
          return {
            message: "Log retrieved successfully",
            data: log
          };
        }
      } else {
        // not a hex string then throw error
        throw Boom.badRequest("Please pass a valid log id");
      }
    }
  }
};

module.exports = logApi;
