var assert = require("assert");
let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../../src/index");
const Log = require("../../src/models/Log");

let should = chai.should();
chai.use(chaiHttp);

describe("Logs", () => {
  describe("Log API requests", () => {
    it("Should be able to add new log", done => {
      chai
        .request(server.listener) // server.listener contains all the server connection data object
        .post("/api/log")
        .send({
          log: {
            title: "my test log",
            description: "this is my test log",
            logPath: "C:/asdasd",
            statusCode: 200
          }
        })
        .end((err, res) => {
          if (err) console.log(err);

          res.should.have.status(200);

          // Check role
          Log.findOne({ title: "my test log" }, (err, doc) => {
            if (err) console.log(err);

            // if no error then log has been saved and the test has passed
          });
          done();
        });
    });

    it("Should be able to get all log", done => {
      chai
        .request(server.listener) // server.listener contains all the server connection data object
        .get("/api/log")
        .end((err, res) => {
          if (err) console.log(err);

          res.should.have.status(200);
          done();
        });
    });

    it("Should be able to get a certian log", done => {
      // get the inserted test log first
      Log.findOne({ title: "my test log" }, (err, doc) => {
        if (err) console.log(err);

        // call the get request with the found doc
        chai
          .request(server.listener) // server.listener contains all the server connection data object
          .get(`/api/log/${doc._id}`)
          .end((err, res) => {
            if (err) console.log(err);

            res.should.have.status(200);
            done();
          });
      });
    });
  });
});

after(done => {
  // delete the created log
  Log.deleteOne(
    {
      title: "my test log"
    },
    err => {
      if (err) console.log(err);
      // finish the test when deleting finishes
      done();
    }
  );
});
